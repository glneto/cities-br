var mongoose = require('mongoose'),
  Promise = require('bluebird'),
  Schema = mongoose.Schema,
  _ = require('lodash'),
  states = require('./states');

//gopro
mongoose.connect('mongodb://heroku_test:573b8b61092b9a2b0c8408e75f7eb45a@ds139869.mlab.com:39869/heroku_n2d7jv1k');

//godev
//mongoose.connect('mongodb://localhost/psadmin');

var City = mongoose.model('City', {
  name: String,
  stateName: String,
  stateShortName: String
});

_.each(states, function (state) {
  console.log('Criando cidades do estado ', state.name, ' com ', state.cities.length, ' cidades.');
  _.each(state.cities, function (city) {
    city.stateName = state.name;
    city.stateShortName = state.shortname;
    return City.create(city, function (err, savedCity) {
      if (err) return console.log(err);
    });
  })
});

